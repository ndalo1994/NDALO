# creating 3d bar plot using matplotlib
# in python

# to interacte with plot matplotlib widget

# importing required libraries
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

# creating random dataset
xs = [20, 30, 40, 50, 10, 60, 20, 100, 700, 200]
ys = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
zs = np.zeros(10)
dx = np.ones(10)
dy = np.ones(10)
dz = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]

# creating figure
figg = plt.figure()
ax = figg.add_subplot(111, projection='3d')

# creating the plot
plot_geeks = ax.bar3d(xs, ys, zs, dx,
					dy, dz, color='green')

# setting title and labels
ax.set_title("3D bar plot")
ax.set_xlabel('x-axis')
ax.set_ylabel('y-axis')
ax.set_zlabel('z-axis')

# displaying the plot
plt.show()




